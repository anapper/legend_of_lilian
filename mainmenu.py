from pick import pick

class MainMenu:
    def __init__(self):
        self.title = 'Legend of Lilian'
        self.options = ['Play', 'Exit']

    def prompt(self):
        options, index = pick('Pick an option')
        if index is self.options.index('Play'):
            pass
        else: 
            exit()