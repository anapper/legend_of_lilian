from pick import pick
import time
import os

def dialog(message):
    os.system('cls')
    print(message)
    input() ## Wait for the user to press enter

# Menu goes here
title = 'Legend of Lilian'
menuoptions = ['Play', 'Exit']
option, index = pick(menuoptions, title)


if option == 'Play':
    # INTRO
    dialog('The sound of rain has woken you')
    dialog('As your eyes struggle to open, you notice the flickering bright light of a flame in the distance towards the end of the road in front of you')
    dialog('You stand up and dust the dirt off your tunic. (Did you fall asleep on the side of the road??)')
    dialog('The night sky is bright and stary. You must have amnesia. You don\'t even remember your own name.')
    dialog('Ahead is a firey torch in the distance and not much light to go by, but the road seems sure')
    dialog('Behind you sounds like a river')
    dialog('There is also the sound of a feint breeze blowing in the nearby trees')
    mainoptions = ['Examine surroundings', 'Inventory', 'Travel']
    option = pick(mainoptions, 'What would you like to do ?')
else:
    exit()